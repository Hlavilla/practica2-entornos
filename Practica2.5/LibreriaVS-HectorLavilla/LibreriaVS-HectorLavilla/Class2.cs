﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_HectorLavilla
{
    public class Class2
    {
        public static void Potencias(int bas, int ex)
        {

            Console.WriteLine(Math.Pow(bas, ex));
        }

        public static void EsPerfecto(int numero) {
            int i, j;
            int sum = 0;

            for (i = 1; i <= numero; i++)
            {
                if (numero % i == 0)
                {
                    j = numero / i;
                    if (j != numero)
                    {
                        sum = sum + j;
                    }

                }
            }
            if (sum == numero) {
                Console.WriteLine("Es perfecto <3");
            }
            else
            {
                Console.WriteLine("No es perfecto.");
            }
        }

        public static void MaximoDeDos(int num1, int num2)
        {
            int result;
            if (num1 > num2)
            {
                result = num1;
            }
            else
            {
                result = num2;
            }
            Console.WriteLine("El mayor es: "+result);
        }

        public static void Log(String g) {
            Console.WriteLine(g);
        }

        public static void Mensaje(String g) {
            Console.WriteLine("Aviso: " + g);
        }
    }
}
