﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibreriaVS_HectorLavilla;

namespace usoLibreriaVS_HectorLavilla
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1.Dividir(2,2);
            Class1.EsPrimo(28);
            Class1.Multipli(2, 2);
            Class1.Resta(2, 6);
            Class1.Suma(2, 6);

            Class2.EsPerfecto(97);
            Class2.Log("Error del sistema");
            Class2.MaximoDeDos(9, 6);
            Class2.Mensaje("Hola");
            Class2.Potencias(2,2);
          
        }
    }
}
