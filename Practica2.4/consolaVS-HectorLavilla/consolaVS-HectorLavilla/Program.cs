﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace consolaVS_HectorLavilla
{
    class Program

    {
        static void Main(string[] args)
        {
            Menu();
        }

        public static void Menu()
        {
            Console.WriteLine("1-suma");
            Console.WriteLine("2-restar");
            Console.WriteLine("3-multiplicar");
            Console.WriteLine("4-dividir");
            Console.WriteLine("5-salir");
            int opcion = 0;
            do
            {
                Console.WriteLine("\nSelecciona una opcion...");
                opcion = LeerTeclado();
            } while (opcion < 1 || opcion>6);

            switch (opcion)
            {
                case 1:
                    Suma(15,4);
                    break;
                case 2:
                    Resta(9,8);
                    break;
                case 3:
                    Multipli(5, 8);
                    break;
                case 4:
                    Dividir(2,1);
                    break;
                case 5:
                    Salir();
                    break;
            }
            Console.WriteLine("\n\nPresiona una tecla para continuar....");
            Console.ReadKey();
            Menu();
        }

        public static void Suma(int x, int y) {
            Console.WriteLine("El resutado de la suma es "+(x+y));
        }

        public static void Resta(int x, int y)
        {
            Console.WriteLine("El resutado de la resta es " + (x - y));
        }

        public static void Multipli(int x, int y)
        {
            Console.WriteLine("El resutado de la multiplicacion es " + (x * y));
        }

        public static void Dividir(int x, int y)
        {
            Console.WriteLine("El resutado de la division es " + (x / y));
        }

        public static void Salir()
        {
            Environment.Exit(0);
        }






        public static int LeerTeclado()
        {
            int leido = 0;
            do
            {
           
                String enter = Console.ReadLine();
                try
                {
                    leido = Int32.Parse(enter);
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }


            } while (leido == 0);
            return leido;

        }
    }
}
