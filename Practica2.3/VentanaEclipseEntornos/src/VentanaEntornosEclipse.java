import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JSpinner;
import javax.swing.JSlider;
import javax.swing.JMenu;
import java.awt.Color;
import javax.swing.JMenuBar;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class VentanaEntornosEclipse extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaEntornosEclipse frame = new VentanaEntornosEclipse();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaEntornosEclipse() {
		setTitle("Tune Bomb");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("");
		label.setBounds(26, 109, 61, 16);
		contentPane.add(label);
		
		JButton btnPlay = new JButton("PLAY");
		btnPlay.addMouseListener(new MouseAdapter() {
			
		});
		btnPlay.setBounds(162, 121, 117, 29);
		contentPane.add(btnPlay);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(VentanaEntornosEclipse.class.getResource("/Fotos/la-clave-de-sol-e1417901981803.jpg")));
		lblNewLabel.setBounds(36, 93, 100, 100);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(VentanaEntornosEclipse.class.getResource("/Fotos/la-clave-de-sol-e1417901981803.jpg")));
		lblNewLabel_1.setBounds(286, 83, 100, 100);
		contentPane.add(lblNewLabel_1);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(211, 211, 211));
		panel.setBounds(6, 6, 438, 65);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblHello = new JLabel("Nombre");
		lblHello.setBounds(16, 6, 61, 16);
		panel.add(lblHello);
		
		textField = new JTextField();
		textField.setBounds(89, 1, 130, 26);
		panel.add(textField);
		textField.setColumns(10);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Es nueva?");
		chckbxNewCheckBox.setBounds(224, 2, 128, 23);
		panel.add(chckbxNewCheckBox);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Rock", "Country", "Pop", "Soul", "Jazz", "Indie", "Salsa"}));
		comboBox.setBounds(328, 2, 104, 27);
		panel.add(comboBox);
		
		JSlider slider = new JSlider();
		slider.setBounds(120, 212, 190, 29);
		contentPane.add(slider);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(322, 212, 33, 26);
		contentPane.add(spinner);
		
		JLabel lblTono = new JLabel("TONO");
		lblTono.setBounds(358, 217, 61, 16);
		contentPane.add(lblTono);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(56, 228, 1, 16);
		contentPane.add(textArea);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(0, 256, 110, 20);
		contentPane.add(toolBar);
		
		JToggleButton tglbtnMute = new JToggleButton("Mute");
		tglbtnMute.setSelected(true);
		toolBar.add(tglbtnMute);
		
		JButton btnGrabar = new JButton("Grabar");
		toolBar.add(btnGrabar);
		
		JRadioButton rdbtnAleatorio = new JRadioButton("Aleatorio");
		rdbtnAleatorio.setBounds(138, 253, 141, 23);
		contentPane.add(rdbtnAleatorio);
	}
}
